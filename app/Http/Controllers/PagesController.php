<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
        return view('pages.dashboard');
    }

    public function login()
    {
        return view('pages.login');
    }

     public function viewol()
    {
        return view('pages.onlinedevice');
    }

    public function viewpen()
    {
        return view('pages.pendingdevice');
    }
}
