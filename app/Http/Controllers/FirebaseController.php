<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Carbon\Carbon;

class FirebaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($class,$id,$lat,$lon)
    {
        return view('pages.dashboard')->with('class',$class);

    }

    public function saveData($class,$id,$lat,$lon)
    {
        $current_date_time = Carbon::now()->toDateTimeString();

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/bus-tracker-1565185373195-firebase-adminsdk-u010c-738c5a4feb.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://bus-tracker-1565185373195.firebaseio.com')
        ->create();

        $database = $firebase->getDatabase();

        $newPost = $database
        ->getReference($class."/".$id)
        ->set([
        'Latitude' => $lat ,
        'Longitude' => $lon,
        'Time-Stamp' => $current_date_time
        ]);
    } 

//   public function passlocation(Request $request){
      

//       return response()->json($request->all());
//   }


}
