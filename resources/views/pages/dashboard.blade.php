@extends('index')

@section('content')
<script src='https://api.tomtom.com/maps-sdk-for-web/5.x/5.35.0/examples/sdk/web-sdk-maps/maps-web.min.js'></script>
<script src="https://api.tomtom.com/maps-sdk-for-web/5.x/5.35.0/examples/pages/examples/assets/js/mobile-or-tablet.js"></script>
<script src="https://api.tomtom.com/maps-sdk-for-web/cdn/5.x/5.35.0/services/services-web.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<div class="container">
        <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); margin:1em;">
                                    <div class="card-header">NSDC Route Bus Monitoring</div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button type="button" class="btn btn-secondary" id="locate" style="margin:1em;">Locate</button>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                            <div id="map"></div>
                                    </div>
                                </div>
                    </div>
        </div>
</div>
<div class="container">
    <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); margin:1em;">
                    <div class="card-header">Bus on Repair</div>
                        <div class="card-body">
                                <h5>100</h5>
                                <p id="test"></p>
                        </div>
                </div>    
            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); margin:1em;">
                        <div class="card-header">Bus on Repair</div>
                        <div class="card-body">
                            <h5>100</h5>
                            <p id="test"></p>
                        </div>
                </div>    
            </div>
        </div>
    </div>


                            {{--------- FIREBASE SAVE --------}}

        <!-- The core Firebase JS SDK is always required and must be listed first -->
        <script src="https://www.gstatic.com/firebasejs/7.2.0/firebase-app.js"></script>
        <script src="https://www.gstatic.com/firebasejs/3.1.0/firebase-database.js"></script>
        <!-- TODO: Add SDKs for Firebase products that you want to use
            https://firebase.google.com/docs/web/setup#available-libraries -->
        <script src="https://www.gstatic.com/firebasejs/7.2.0/firebase-analytics.js"></script>

        <script>
        // Your web app's Firebase configuration
        var firebaseConfig = {
            apiKey: "AIzaSyA2tRArjSIDBJBEd0iMgkC5nxX5GBUKVe0",
            authDomain: "bus-tracker-1565185373195.firebaseapp.com",
            databaseURL: "https://bus-tracker-1565185373195.firebaseio.com",
            projectId: "bus-tracker-1565185373195",
            storageBucket: "bus-tracker-1565185373195.appspot.com",
            messagingSenderId: "55578199241",
            appId: "1:55578199241:web:9449fe1c1d1b4ea6e6f918",
            measurementId: "G-GEFZ9DW7GJ"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
        firebase.analytics();
        </script>

        <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
        <script src="{{asset('js/index.js')}}"></script>
           
           <script>
                    // var markers = [];
                    // var rtlat = document.getElementById('loC').val; 

                    var loc = [121.0075, 14.8743];
                    var map = tt.map({
                    key: '3mzAR46ROij2Xz9vU96j8ASonhR2NMrI',
                    container: 'map',
                    style: 'tomtom://vector/1/basic-main',
                    center: loc,
                    zoom:14,
                    dragPan: !window.isMobileOrTablet()
                    });
                    map.addControl(new tt.FullscreenControl());
                    map.addControl(new tt.NavigationControl());
                    
                    // $.get('passmylocation',function(data){
                    //     console.log(data)
                    // })

                    
                    // function AddBus(data){

                    //     var uluru = {asd123: data.val().Longitude,asd123: data.val().Latitude};
                    //     console.log(uluru);
                    //     var marker = new tt.Marker()
                    //     .setLngLat(uluru)
                    //     .addTo(map);


                    //     markers[data.key] = marker;
                    // };

                    // var dbRef = firebase.database().ref('Class A');


                    // dbRef.on('child_added', function (data){
                    //     AddBus(data);
                    // });

                    // dbRef.on('child_changed', function(data){
                    //     markers[data.key].setMap(null);
                    //     AddBus(data);
                    // });

                    // dbRef.on('child_removed' , function(data){
                    //     markers[data.key].setMap(null);

                    // });

                
        
                </script>
@endsection
