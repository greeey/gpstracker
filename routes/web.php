<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/firebase/{class}/{id}&lat={lat}&lon={lon}','FirebaseController@saveData');
Route::get('/login' , 'PagesController@login');
Route::get('/viewol' , 'PagesController@viewol');
Route::get('/viewpen' , 'PagesController@viewpen');